# ISAN Navi


## Beschreibung
Es gibt mehrere Ansätze in Starbase eine Navigation zu einer bestimmten ISAN Position zu ermöglichen.

Diese hier ist nicht die stylischste und auch nicht die technisch beste Umsetzung.

Der Vorteil dieser Version ist, dass sie simpel und einigermaßen schnell ist. (schnell ist ziemlich relativ bei 0.2sec/line)

![](anzeige.PNG)

Es werden 2 ISAN System in einer geraden Linie zum Schiff montiert, eins vorne und eins hinten.

Ein großzügiger Abstand zwischen den beiden ISAN ist von Vorteil. 


## Was du brauchst:
- 3 adv. YOLOL Chips
- 2 Receivers (Mono ISAN) / 8 Reciver (Quad ISAN)
- 6 Text Panels
- 1 Memory Chip (6 freie Plätze)


## YOLOL Code
- [ISAN A (Front)](https://gitlab.com/dennis.drochmann/isan-navi/-/blob/main/ISAN_A.yolol)
- [ISAN B (Back)](https://gitlab.com/dennis.drochmann/isan-navi/-/blob/main/ISAN_B.yolol)
- [Navi](https://gitlab.com/dennis.drochmann/isan-navi/-/blob/main/navi.yolol)


## Installation
Die Receiver müssen in einer geraden Linie zu deinem Schiff aufgebaut sein, ein ISAN vorne und ein ISAN hinten.

Ob du nun eine MONO oder QUAD ISAN benutzt bleibt dir überlassen.

Man sollte aber vorne und hinten die gleiche Art benutzen um asynchronität zu vermeiden.

(Meine favorisierte Version ist QUAD mit ausgeschaltetem Speed, da diese Version wohl am schnellsten läuft.)


Mein Code ist etwas abgewandelt, aber im Prinzip sind die ISAN genauso wie hier beschrieben: https://isan.to/isan.pdf

Ich setze hier dieses Grundwissen von ISAN voraus, orientiert euch an der orginal Beschreibung.


Das sind die Variablen, wie sie **in angefügten ISAN Scripts schon angepasst** sind:

Der vordere ISAN ist wie ein normaler ISAN gebaut, natürlich mit globals erweitert.
- Receiver Variablen :a :at :b :bt :c :ct :d :dt
- ISAN Output Variablen :x :y :z

Der hintere ISAN ist natürlich etwas angepasst.
- Receiver Variablen :e :et :f :ft :g :gt :h :ht
- ISAN Output Variablen :i :j :k

2 Panels für die beiden ISAN Ausgaben, mit den Variablen Namen :m :n

Im Memorychip müssen :x :y :z :i :j :k vorhanden sein.

3 Panels in die man später die Zielcoordinaten eintragt, mit den Variablen Namen :dx :dy :dz

1 Panel für die Navigationsanzeige, mit den Variablen Namen :navi


## Verwendung
Wenn alles läuft, dann hat man auf der Anzeige **keine** tolle Grafik.

_(Ich habe es mit grafischer Anzeige probiert, macht alles nur unnötig träge und für gute Anzeigen sind kompliziertere Aufbauten/Berechnungen nötig.)_

Man hat eine Zahl die sich irgendwo zwischen 10000 und -10000 bewegt. _(Das ist anhänig vom Abstand der zwei ISAN)_

Ja genau, das ist unsere Navigation. :D

Nun ist es am Menschen diesen Wert auf 0 zu bekommen.

Man dreht das Schiff vorsichtig in eine Richtung, sagen wir mal in der seitlichen Achse und guckt ob der Wert sich an 0 annähert.

Sollte er das nicht tun, nimmt man die andere Richtung.

Hat man den nächsten Wert zu 0 auf dieser Achse erreicht, wechselt man zur höhen Achse und macht da das gleiche.

Werte die unter +-10 liegen, sind schon recht gut, besser wäre unter +-5 und am genauesten natürlich wenn man +-1 schafft.

Das klingt schwieriger als es ist, mit ein wenig Gefühl und Übung _(aber auch je nach Schiff)_ richtet man sich in ca. 20 Sekunden aus.

Wichtig ist, dabei sollte das Schiff still stehen **(auch die Drehung verfälscht)** und der aktualisierte Wert kommt immer um eine halbe Sekunde verzögert.


Die untere Anzeige zeigt lediglich die Distance, damit man weiß wie weit das Ziel noch entfernt ist.


Man könnte natürlich noch eine Anzeige dazu bauen oder versuchen die beiden ISAN genau zu synchronisieren _(was sicherlich Sinn macht)_ und vieles mehr.

Aber ich wollte das hier so einfach wie möglich halten.


## Beispiel ISAN

![](testschiff.PNG)
